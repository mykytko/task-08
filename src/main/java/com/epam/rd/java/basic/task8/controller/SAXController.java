package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private final String xmlFileName;
	private List<Flower> flowers;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flower> parseDocument() throws ParserConfigurationException, SAXException, IOException {
		SAXParserFactory parserFactory = SAXParserFactory.newInstance();
		SAXParser parser = parserFactory.newSAXParser();
		SAXHandler handler = new SAXHandler();
		flowers = new ArrayList<>();
		parser.parse(xmlFileName, handler);
		return flowers;
	}

	class SAXHandler extends DefaultHandler {
		Flower flower = null;
		String content = null;
		String attribute = null;

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) {
			if (qName.equals("flower")) {
				flower = new Flower();
			}
			attribute = attributes.getValue(0);
		}

		@Override
		public void endElement(String uri, String localName, String qName) {
			if (qName.equals("flower")) {
				flowers.add(flower);
			} else {
				CommonController.assign(flower, qName, attribute, content);
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) {
			content = String.copyValueOf(ch, start, length).trim();
		}
	}

}