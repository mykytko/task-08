package com.epam.rd.java.basic.task8.model;

public class Flower {
    public String name;
    public String soil;
    public String origin;
    public VisualParameters visualParameters;
    public GrowingTips growingTips;
    public String multiplying;
}


