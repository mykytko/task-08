package com.epam.rd.java.basic.task8.model;

public class VisualParameters {
    public String stemColour;
    public String leafColour;
    public AveLenFlower aveLenFlower;
}
