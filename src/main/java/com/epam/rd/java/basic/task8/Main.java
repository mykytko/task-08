package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.model.Flower;

import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		String xsdFileName = xmlFileName.replaceAll("xml$", "xsd");
		CommonController.validateXML(xmlFileName, xsdFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		List<Flower> flowers = domController.parseDocument();

		// sort (case 1)
		CommonController.sortByName(flowers);

		// save
		String outputXmlFile = "output.dom.xml";
		CommonController.saveToXML(flowers, outputXmlFile);
		CommonController.validateXML(outputXmlFile, xsdFileName);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		flowers = saxController.parseDocument();

		// sort  (case 2)
		CommonController.sortBySoil(flowers);

		// save
		outputXmlFile = "output.sax.xml";
		CommonController.saveToXML(flowers, outputXmlFile);
		CommonController.validateXML(outputXmlFile, xsdFileName);

		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		flowers = staxController.parseDocument();

		// sort  (case 3)
		CommonController.sortByOrigin(flowers);

		// save
		outputXmlFile = "output.stax.xml";
		CommonController.saveToXML(flowers, outputXmlFile);
		CommonController.validateXML(outputXmlFile, xsdFileName);
	}

}
