package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Flower;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamConstants;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private List<Flower> flowers;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flower> parseDocument() throws XMLStreamException, FileNotFoundException {
		Flower flower = null;
		String content = null;
		String attribute = null;
		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLStreamReader reader = factory.createXMLStreamReader(new FileReader(xmlFileName));

		while (reader.hasNext()) {
			int event = reader.next();
			switch(event) {
				case XMLStreamConstants.START_ELEMENT:
					if (reader.getLocalName().equals("flower")) {
						flower = new Flower();
					}
					if (reader.getLocalName().equals("flowers")) {
						flowers = new ArrayList<>();
					}
					attribute = reader.getAttributeValue(0);
					break;
				case XMLStreamConstants.CHARACTERS:
					content = reader.getText().trim();
					break;
				case XMLStreamConstants.END_ELEMENT:
					CommonController.assign(flower, reader.getLocalName(), attribute, content);
					break;
				case XMLStreamConstants.START_DOCUMENT:
					flowers = new ArrayList<>();
					break;
			}

		}

		return flowers;
	}

}