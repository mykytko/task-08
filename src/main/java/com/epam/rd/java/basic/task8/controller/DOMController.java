package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.model.Flower;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private final String xmlFileName;
	private List<Flower> flowers;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flower> parseDocument() throws ParserConfigurationException, IOException, SAXException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(xmlFileName);

		flowers = new ArrayList<>();
		NodeList nodeList = document.getDocumentElement().getLastChild().getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if (node instanceof Element) {
				Flower flower = new Flower();
				NodeList childNodes = node.getChildNodes();
				for (int j = 0; j < childNodes.getLength(); j++) {
					Node childNode = childNodes.item(j);
					parseNode(flower, childNode);
					if (childNode.hasChildNodes()) {
						NodeList gcNodes = childNode.getChildNodes();
						for (int k = 0; k < gcNodes.getLength(); k++) {
							parseNode(flower, gcNodes.item(k));
						}
					}
				}
				flowers.add(flower);
			}
		}
		return flowers;
	}

	private void parseNode(Flower flower, Node childNode) {
		if (childNode instanceof Element) {
			String content = childNode.getTextContent();
			String nodeName = childNode.getNodeName();
			Node attr = childNode.getAttributes().item(0);
			String attribute = null;
			if (attr != null) {
				attribute = attr.getNodeValue();
			}
			CommonController.assign(flower, nodeName, attribute, content);
		}
	}

}
