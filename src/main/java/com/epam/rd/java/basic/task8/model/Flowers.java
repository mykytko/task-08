package com.epam.rd.java.basic.task8.model;

import java.util.List;

public class Flowers {
    public List<Flower> flower;
    public String xmlns;
    public String xsi;
    public String schemaLocation;
    public String text;
}
