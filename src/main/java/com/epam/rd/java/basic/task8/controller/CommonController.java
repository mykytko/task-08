package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.*;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class CommonController {
    public static void sortByName(List<Flower> flowers) {
        flowers.sort((o1, o2) -> o1.name.compareToIgnoreCase(o2.name));
    }

    public static void sortBySoil(List<Flower> flowers) {
        flowers.sort((o1, o2) -> o1.soil.compareToIgnoreCase(o2.soil));
    }

    public static void sortByOrigin(List<Flower> flowers) {
        flowers.sort((o1, o2) -> o1.origin.compareToIgnoreCase(o2.origin));
    }

    public static void saveToXML(List<Flower> flowers, String fileName) throws IOException {
        XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
        try (FileWriter fileWriter = new FileWriter(fileName)) {
            XMLStreamWriter xmlStreamWriter = outputFactory.createXMLStreamWriter(fileWriter);
            xmlStreamWriter.writeStartDocument("UTF-8", "1.0");

            xmlStreamWriter.writeStartElement("flowers");
            xmlStreamWriter.writeAttribute("xmlns", "http://www.nure.ua");
            xmlStreamWriter.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            xmlStreamWriter.writeAttribute("xmlns:schemaLocation", "http://www.nure.ua input.xsd ");

            for (var flower: flowers) {
                xmlStreamWriter.writeStartElement("flower");

                xmlStreamWriter.writeStartElement("name");
                xmlStreamWriter.writeCharacters(flower.name);
                xmlStreamWriter.writeEndElement();

                xmlStreamWriter.writeStartElement("soil");
                xmlStreamWriter.writeCharacters(flower.soil);
                xmlStreamWriter.writeEndElement();

                xmlStreamWriter.writeStartElement("origin");
                xmlStreamWriter.writeCharacters(flower.origin);
                xmlStreamWriter.writeEndElement();

                xmlStreamWriter.writeStartElement("visualParameters");

                xmlStreamWriter.writeStartElement("stemColour");
                xmlStreamWriter.writeCharacters(flower.visualParameters.stemColour);
                xmlStreamWriter.writeEndElement();

                xmlStreamWriter.writeStartElement("leafColour");
                xmlStreamWriter.writeCharacters(flower.visualParameters.leafColour);
                xmlStreamWriter.writeEndElement();

                xmlStreamWriter.writeStartElement("aveLenFlower");
                xmlStreamWriter.writeAttribute("measure", flower.visualParameters.aveLenFlower.measure);
                xmlStreamWriter.writeCharacters(Integer.toString(flower.visualParameters.aveLenFlower.text));
                xmlStreamWriter.writeEndElement();

                xmlStreamWriter.writeEndElement();

                xmlStreamWriter.writeStartElement("growingTips");

                xmlStreamWriter.writeStartElement("temperature");
                xmlStreamWriter.writeAttribute("measure", flower.growingTips.temperature.measure);
                xmlStreamWriter.writeCharacters(Integer.toString(flower.growingTips.temperature.text));
                xmlStreamWriter.writeEndElement();

                xmlStreamWriter.writeStartElement("lighting");
                xmlStreamWriter.writeAttribute("lightRequiring", flower.growingTips.lighting.lightRequiring);
                xmlStreamWriter.writeEndElement();

                xmlStreamWriter.writeStartElement("watering");
                xmlStreamWriter.writeAttribute("measure", flower.growingTips.watering.measure);
                xmlStreamWriter.writeCharacters(Integer.toString(flower.growingTips.watering.text));
                xmlStreamWriter.writeEndElement();

                xmlStreamWriter.writeEndElement();

                xmlStreamWriter.writeStartElement("multiplying");
                xmlStreamWriter.writeCharacters(flower.multiplying);
                xmlStreamWriter.writeEndElement();

                xmlStreamWriter.writeEndElement();
            }

            xmlStreamWriter.writeEndElement();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }

    public static void validateXML(String xml, String xsd) throws ParserConfigurationException, IOException, SAXException {
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Source schemaFile = new StreamSource(new File(xsd));
        Schema schema = schemaFactory.newSchema(schemaFile);
        Validator validator = schema.newValidator();

        try {
            validator.validate(new StreamSource(xml));
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    public static void assign(Flower flower, String nodeName, String attribute, String content) {
        switch (nodeName) {
            case "name":
                flower.name = content;
                break;
            case "soil":
                flower.soil = content;
                break;
            case "origin":
                flower.origin = content;
                break;
            case "visualParameters":
                if (flower.visualParameters == null) {
                    flower.visualParameters = new VisualParameters();
                }
                break;
            case "stemColour":
                if (flower.visualParameters == null) {
                    flower.visualParameters = new VisualParameters();
                }
                flower.visualParameters.stemColour = content;
                break;
            case "leafColour":
                if (flower.visualParameters == null) {
                    flower.visualParameters = new VisualParameters();
                }
                flower.visualParameters.leafColour = content;
                break;
            case "aveLenFlower":
                if (flower.visualParameters == null) {
                    flower.visualParameters = new VisualParameters();
                }
                flower.visualParameters.aveLenFlower = new AveLenFlower();
                flower.visualParameters.aveLenFlower.measure = attribute;
                flower.visualParameters.aveLenFlower.text = Integer.parseInt(content);
                break;
            case "growingTips":
                if (flower.growingTips == null) {
                    flower.growingTips = new GrowingTips();
                }
                break;
            case "temperature":
                if (flower.growingTips == null) {
                    flower.growingTips = new GrowingTips();
                }
                flower.growingTips.temperature = new Temperature();
                flower.growingTips.temperature.measure = attribute;
                flower.growingTips.temperature.text = Integer.parseInt(content);
                break;
            case "lighting":
                if (flower.growingTips == null) {
                    flower.growingTips = new GrowingTips();
                }
                flower.growingTips.lighting = new Lighting();
                flower.growingTips.lighting.lightRequiring = attribute;
                break;
            case "watering":
                if (flower.growingTips == null) {
                    flower.growingTips = new GrowingTips();
                }
                flower.growingTips.watering = new Watering();
                flower.growingTips.watering.measure = attribute;
                flower.growingTips.watering.text = Integer.parseInt(content);
                break;
            case "multiplying":
                flower.multiplying = content;
        }
    }
}
